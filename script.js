'use strict';

/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?

	Event - це згенерований сигнал, що щось відбулось. Використовуються для того, щоб по тій чи іншій події виконати якусь дію.

2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.

	Для миші доступні такі події: mouseover/mouseout - коли курсор наводиться на елемент, або забирається з елементу, mousemove - рухи курсора, click - клік по елементу, mousedown/mouseup - момент натискання та відпускання кнопки, dblclick - подвійний клік на елементі, contextmenu - клік по елементу правою кнопкою миші.

3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

	Подія "contextmenu" викликається натисканням правої кнопки миші і викликає контекстне меню для елементу, також викликати можна спеціальною клавішою "контекстного меню" на клавіатурі.
*/

/* 
Практичні завдання
1. Додати новий абзац по кліку на кнопку:
По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
*/

const btn = document.getElementById('btn-click');

btn.addEventListener("click", function (e) {
	btn.insertAdjacentHTML("beforebegin", '<p>New Paragraph</p>');
});

/* 
2. Додати новий елемент форми із атрибутами:
Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
*/


const content = document.getElementById('content');

// const newContainer = document.createElement('div');
// newContainer.setAttribute('id', 'mouse-event-div');
// content.after(newContainer);
// const button = document.createElement('button');
// button.setAttribute('id', 'btn-input-create');
// button.innerText = 'Create Input';
// newContainer.append(button);

const newDiv = `<div id="mouse-event-div"><button id="btn-input-create">Create Input</button></div>`;
content.insertAdjacentHTML('afterend', newDiv);
const button = document.getElementById('btn-input-create');

button.addEventListener("click", function (e) {
	const input = `<input id="input-text"
						type="text" name="text"
						placeholder="write here"></input>`;
	
	button.insertAdjacentHTML('afterend', input);
});

/* 

*/